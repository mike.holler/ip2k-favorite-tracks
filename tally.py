from bs4 import BeautifulSoup
from pathlib import Path
from itertools import chain
from dataclasses import dataclass
from typing import List

paid_na_tracks = [
    "Barber Motorsports Park",
    "COTA (Circuit of the Americas)",
    "Canadian Tire Motorsports Park",
    "Circuit Gilles Villeneuve",
    "Daytona International Speedway (Roval)",
    "Detroit Grand Prix at Belle Isle",
    "Homestead Motor Speedway (Roval)",
    "Indianapolis Motor Speedway (Roval)",
    "Kansas Speedway (Roval)",
    "Long Beach",
    "Mid-Ohio Sports Car Course",
    "New Hampshire Motor Speedway (Roval)",
    "Road America",
    "Road Atlanta",
    "Sebring International Raceway",
    "Sonoma Raceway",
    "VIR (Virginia International Raceway)",
    "Watkins Glen International",
    "World Wide Technology Raceway (Roval)",
]

paid_intl_tracks = [
    "Autodromo Nazionale Monza",
    "Brands Hatch Circuit",
    "Circuit Park Zandvoort",
    "Circuit Zolder",
    "Circuit de Barcelona Catalunya",
    "Circuit de Spa-Francorchamps",
    "Circuit des 24 Heures du Mans (Le Mans)",
    "Donington Park Racing Circuit",
    "Imola (Autodromo Internazionale Enzo e Dino Ferrari)",
    "Interlagos (Autódromo José Carlos Pace)",
    "Mount Panorama Circuit",
    "Nurburgring Nordschleife",
    "Nürburgring Grand-Prix-Strecke",
    "Phillip Island Circuit",
    "Silverstone Circuit",
    "Snetterton Circuit",
    "Suzuka International Racing Course",
]

free_tracks = [
    "Charlotte Motor Speedway (Roval)",
    "Lime Rock Park",
    "Okayama International Circuit",
    "Oran Park Raceway",
    "Oulton Park Circuit",
    "Summit Point Raceway",
    "Tsukuba Circuit",
    "WeatherTech Raceway at Laguna Seca",
    "[Legacy] Daytona International Speedway - 2008 (Roval)",
    "[Legacy] Silverstone Circuit - 2008",
]

all_tracks = paid_na_tracks + paid_intl_tracks + free_tracks


@dataclass
class Post:
    author: str
    text: str


@dataclass
class Ballot:
    voter: str
    tracks: List[str]
    original_post: Post


@dataclass
class TrackWithVotes:
    track: str
    votes: int


def get_posts(page: Path) -> List[Post]:
    soup = BeautifulSoup(markup=page.read_text("utf-8"))
    rows = soup.find_all(class_="trPosts")
    return [
        Post(
            author=r.find(class_="tdPostAuthor").find("a").text,
            text=r.find(class_="postBody").text.strip(),
        ) for r in rows
    ]


def post_contains_a_track_name(post: Post) -> bool:
    for track in all_tracks:
        if track in post.text:
            return True
    return False


def format_ballot(text: str) -> List[str]:
    """
    Turn an unstructured ballot into a list of tracks contained in the vote.

    Maintains original order of tracks in vote.
    """
    track_and_order = sorted(
        [(t, text.find(t)) for t in all_tracks],
        key=lambda x: x[1]
    )

    return [to[0] for to in track_and_order if to[1] != -1]


def tally_votes(ballots: List[Ballot]) -> List[TrackWithVotes]:
    """
    Return a list of all tracks with their votes attached.
    """
    results = []
    for track in all_tracks:
        results.append(TrackWithVotes(
            track=track,
            votes=len([b for b in ballots if track in b.tracks])
        ))

    return results


def print_header(text: str):
    print("=" * 79)
    print(text)
    print("=" * 79)


def print_results(results):
    one_third_count = len(results) // 3
    high_popularity = results[:one_third_count]
    mid_popularity = results[one_third_count:one_third_count * 2]
    low_popularity = results[one_third_count * 2:]

    print("High Popularity Tracks (votes)")
    for t in high_popularity:
        print(f"  {t.track} ({t.votes})")
    print()

    print("Average Popularity Tracks (votes)")
    for t in mid_popularity:
        print(f"  {t.track} ({t.votes})")
    print()

    print("Low Popularity Tracks (votes)")
    for t in low_popularity:
        print(f"  {t.track} ({t.votes})")
    print()


def main():
    files = sorted(list(Path(Path(__file__).parent, "data").glob("*.html")))
    # Gather all posts.
    posts = list(chain.from_iterable(get_posts(f) for f in files))
    # Exclude first post.
    posts = posts[1:]
    # If the post contains a track name, it is counted as a "vote post".
    vote_posts = [post for post in posts if post_contains_a_track_name(post)]
    # Get a list of voters.
    voters = sorted(set([p.author for p in posts]))
    # Gather a voter's ballots in order of submission.
    ballots_by_voter = {
        voter: [post for post in vote_posts if post.author == voter]
        for voter in voters
    }
    # Ballots by voter that actually voted, rather than just commenting.
    ballots_by_voter = {k: v for k, v in ballots_by_voter.items() if len(v) > 0}
    # Some voters voted multiple times. We need to eliminate unfairnes by only
    # accepting the most recent vote.
    valid_ballots = [ballots[-1] for voter, ballots in ballots_by_voter.items()]
    # Convert the ballots from plain text into a list of track votes.
    formatted_ballots = [
        Ballot(
            voter=b.author,
            tracks=format_ballot(b.text),
            original_post=b
        )
        for b in valid_ballots
    ]
    # Limit ballots to 15 votes by only taking the first 15 tracks mentioned,
    # and also don't count double votes for the same track.
    for ballot in formatted_ballots:
        ballot.tracks = list(set(ballot.tracks[:15]))

    raw_results = sorted(
        tally_votes(formatted_ballots),
        key=lambda x: x.votes,
        reverse=True
    )

    paid_na_results = [t for t in raw_results if t.track in paid_na_tracks]
    paid_intl_results = [t for t in raw_results if t.track in paid_intl_tracks]
    free_results = [t for t in raw_results if t.track in free_tracks]

    print_header("ALL TRACKS")
    print_results(raw_results)
    print()

    print_header("NORTH AMERICAN TRACKS")
    print_results(paid_na_results)
    print()

    print_header("INTERNATIONAL TRACKS")
    print_results(paid_intl_results)
    print()

    print_header("FREE TRACKS")
    print_results(free_results)
    print()


if __name__ == "__main__":
   main()
