import hashlib

from sys import argv
from pathlib import Path


def main(csv_path: Path, secret: str):
    """
    Convert a two-column CSV file (custid, name) into a csv with headers:

        custid, name, ballot_id

    Assumes input CSV has header row.
    """
    raw_rows = [
        line.split(",")[:2]
        for line in csv_path.read_text(encoding="utf-8").splitlines()[1:]
        if "," in line
    ]

    custid_to_name = {int(r[0]): r[1] for r in raw_rows}

    ballot_ids = {
        custid: hashlib.sha1(f"{custid}|{secret}".encode("utf-8")).hexdigest()
        for custid in custid_to_name.keys()
    }

    standard_csv = "custid,name,ballot_id\n" + "\n".join([
        ",".join([str(custid), name, ballot_ids[custid]])
        for custid, name in custid_to_name.items()
    ])

    pmtool_csv = "\n".join([
        ";".join([str(custid), ballot_ids[custid]])
        for custid, name in custid_to_name.items()
    ])

    Path(
        csv_path.parent,
        f"{csv_path.stem}-with-secrets-{secret}.csv"
    ).write_text(standard_csv)

    Path(
        csv_path.parent,
        f"{csv_path.stem}-with-secrets-{secret}-pmtool.csv"
    ).write_text(pmtool_csv)


if __name__ == "__main__":
    main(csv_path=Path(argv[1]), secret=argv[2])
