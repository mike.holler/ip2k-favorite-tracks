```
===============================================================================
ALL TRACKS
===============================================================================
High Popularity Tracks (votes)
  Watkins Glen International (81)
  Road America (79)
  Road Atlanta (75)
  Sebring International Raceway (71)
  Long Beach (69)
  Circuit de Spa-Francorchamps (67)
  Canadian Tire Motorsports Park (65)
  Detroit Grand Prix at Belle Isle (61)
  Mid-Ohio Sports Car Course (61)
  Interlagos (Autódromo José Carlos Pace) (58)
  Brands Hatch Circuit (55)
  Suzuka International Racing Course (55)
  WeatherTech Raceway at Laguna Seca (54)
  Circuit Gilles Villeneuve (52)
  Barber Motorsports Park (50)

Average Popularity Tracks (votes)
  Autodromo Nazionale Monza (45)
  VIR (Virginia International Raceway) (42)
  Circuit Park Zandvoort (41)
  Imola (Autodromo Internazionale Enzo e Dino Ferrari) (41)
  Phillip Island Circuit (41)
  Mount Panorama Circuit (40)
  Nurburgring Nordschleife (39)
  Circuit de Barcelona Catalunya (37)
  Sonoma Raceway (36)
  Oulton Park Circuit (36)
  Indianapolis Motor Speedway (Roval) (35)
  Donington Park Racing Circuit (33)
  COTA (Circuit of the Americas) (31)
  Lime Rock Park (30)
  Silverstone Circuit (27)

Low Popularity Tracks (votes)
  Okayama International Circuit (26)
  Daytona International Speedway (Roval) (24)
  Charlotte Motor Speedway (Roval) (24)
  Nürburgring Grand-Prix-Strecke (22)
  Summit Point Raceway (21)
  Circuit Zolder (19)
  Homestead Motor Speedway (Roval) (18)
  Circuit des 24 Heures du Mans (Le Mans) (18)
  [Legacy] Silverstone Circuit - 2008 (11)
  Tsukuba Circuit (10)
  New Hampshire Motor Speedway (Roval) (9)
  Snetterton Circuit (8)
  Oran Park Raceway (7)
  World Wide Technology Raceway (Roval) (5)
  Kansas Speedway (Roval) (2)
  [Legacy] Daytona International Speedway - 2008 (Roval) (2)


===============================================================================
NORTH AMERICAN TRACKS
===============================================================================
High Popularity Tracks (votes)
  Watkins Glen International (81)
  Road America (79)
  Road Atlanta (75)
  Sebring International Raceway (71)
  Long Beach (69)
  Canadian Tire Motorsports Park (65)

Average Popularity Tracks (votes)
  Detroit Grand Prix at Belle Isle (61)
  Mid-Ohio Sports Car Course (61)
  Circuit Gilles Villeneuve (52)
  Barber Motorsports Park (50)
  VIR (Virginia International Raceway) (42)
  Sonoma Raceway (36)

Low Popularity Tracks (votes)
  Indianapolis Motor Speedway (Roval) (35)
  COTA (Circuit of the Americas) (31)
  Daytona International Speedway (Roval) (24)
  Homestead Motor Speedway (Roval) (18)
  New Hampshire Motor Speedway (Roval) (9)
  World Wide Technology Raceway (Roval) (5)
  Kansas Speedway (Roval) (2)


===============================================================================
INTERNATIONAL TRACKS
===============================================================================
High Popularity Tracks (votes)
  Circuit de Spa-Francorchamps (67)
  Interlagos (Autódromo José Carlos Pace) (58)
  Brands Hatch Circuit (55)
  Suzuka International Racing Course (55)
  Autodromo Nazionale Monza (45)

Average Popularity Tracks (votes)
  Circuit Park Zandvoort (41)
  Imola (Autodromo Internazionale Enzo e Dino Ferrari) (41)
  Phillip Island Circuit (41)
  Mount Panorama Circuit (40)
  Nurburgring Nordschleife (39)

Low Popularity Tracks (votes)
  Circuit de Barcelona Catalunya (37)
  Donington Park Racing Circuit (33)
  Silverstone Circuit (27)
  Nürburgring Grand-Prix-Strecke (22)
  Circuit Zolder (19)
  Circuit des 24 Heures du Mans (Le Mans) (18)
  Snetterton Circuit (8)


===============================================================================
FREE TRACKS
===============================================================================
High Popularity Tracks (votes)
  WeatherTech Raceway at Laguna Seca (54)
  Oulton Park Circuit (36)
  Lime Rock Park (30)

Average Popularity Tracks (votes)
  Okayama International Circuit (26)
  Charlotte Motor Speedway (Roval) (24)
  Summit Point Raceway (21)

Low Popularity Tracks (votes)
  [Legacy] Silverstone Circuit - 2008 (11)
  Tsukuba Circuit (10)
  Oran Park Raceway (7)
  [Legacy] Daytona International Speedway - 2008 (Roval) (2)
```
